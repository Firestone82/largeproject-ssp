# SSP Project - Large

Authors: Mikula Pavel, Osoba Miroslav

## Instalation

1. Clone this repository - `git clone https://gitlab.com/Firestone82/largeproject-ssp.git`
2. Install required packages - `pip install -r requirements.txt`

## Usage
1. Run program - `python source.py`
2. Test program - `python -m pytest -v test.py`
